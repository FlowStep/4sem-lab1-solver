import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class InterpTest {
    private String failTest(double expected, double taken) {
        String res = "failed - " + Double.toString(expected) + " expected but " + Double.toString(taken) + " taken";
        return res;
    }

    @Test
    public void testInterp() {
        double[] x = new double[] {1, 4, 7, 10};
        double[] f = new double[] {6, 3, 8, 1 };
        Interp interpolation = new Interp(x, f);

        for (int i = 0; i < 4; ++i) {
            double fi = interpolation.getValue(x[i]);
            assertTrue(failTest(f[i], fi), MyDouble.equals(f[i], fi));
        }
    }
}
