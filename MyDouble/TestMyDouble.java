import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TestMyDouble {
    @Test
    public void testMyDoubleEquals() {
        assertTrue("failure - should be true", MyDouble.equals(5.000000, 4.999998));
        assertTrue("failure - should be true", MyDouble.equals(2.142857142857143, 2.142857142857143));
        assertTrue("failure - should be true", MyDouble.equals(1.428571428571429, 1.4285714285714286));
        assertTrue("failure - should be true", MyDouble.equals(0.714285714285714, 0.7142857142857143));
        assertFalse("failure - should be false", MyDouble.equals(5.000000, 4.999887));
    }

    @Test
    public void testMyDoubleEquals3args() {
        assertTrue("failure - should be true", MyDouble.equals(5.00121, 5.00122, 0.0001));
        assertFalse("failure - should be false", MyDouble.equals(5.00121, 5.00132, 0.0001));
    }

    @Test
    public void testMyDoubleGreaterThan() {
        assertTrue("failure - should be true", MyDouble.greaterThan(5.000000, 4.999887));
        assertFalse("failure - should be false", MyDouble.greaterThan(5.000000, 4.999998));
        assertFalse("failure - should be false", MyDouble.greaterThan(4.999887, 5.000000));
    }

    @Test
    public void testMyDoubleGreaterThan3args() {
        assertTrue("failure - should be true", MyDouble.greaterThan(5.013, 5.012, 0.0001));
        assertFalse("failure - should be false", MyDouble.greaterThan(5.012, 5.012, 0.0001));
        assertFalse("failure - should be ", MyDouble.greaterThan(5.013, 5.014, 0.0001));
    }

    @Test
    public void testMyDoubleLessThan() {
        assertTrue("failure - should be true", MyDouble.lessThan(4.999887, 5.000000));
        assertFalse("failure - should be false", MyDouble.lessThan(4.999998, 5.000000));
        assertFalse("failure - should be false", MyDouble.lessThan(5.000000, 4.999887));
    }

    @Test
    public void testMyDoubleLessThan3args() {
        assertTrue("failure - should be true", MyDouble.lessThan(5.011, 5.012, 0.0001));
        assertFalse("failure - should be false", MyDouble.lessThan(5.011111, 5.0112, 0.0001));
        assertFalse("failure - should be false", MyDouble.lessThan(5.012, 5.011, 0.0001));
    }
}
