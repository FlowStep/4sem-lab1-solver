public class MyDouble {
    private final static double EPSILON = 0.00001;

    public static boolean equals(double left, double right) {
        return left == right ? true : Math.abs(left - right) < EPSILON;
    }

    public static boolean equals(double left, double right, double epsilon) {
        return left == right ? true : Math.abs(left - right) < epsilon;
    }

    public static boolean greaterThan(double left, double right, double epsilon) {
        return left - right > epsilon;
    }

    public static boolean greaterThan(double left, double right) {
        return greaterThan(left, right, EPSILON);
    }

    public static boolean lessThan(double left, double right, double epsilon) {
        return right - left > epsilon;
    }

    public static boolean lessThan(double left, double right) {
        return lessThan(left, right, EPSILON);
    }
}
