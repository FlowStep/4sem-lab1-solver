public class Polynom {
    protected double[] coefs;

    public Polynom(double[] coefs_) {
        coefs = coefs_;
    }

    public Polynom() {
        // Маленький костыль
    }

    public double getValue(double x) {
        double res = coefs[0];

        for (int i = 1; i < coefs.length; ++i)
            res += coefs[i] * Math.pow(x, i);

        return res;
    }

    public double[] getCoefficients() {
        return Arrays.copyOf(coefs, coefs.length);
    }
}
