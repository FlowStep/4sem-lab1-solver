import static org.junit.Assert.assertTrue;
import org.junit.Test;
import Jama.Matrix;
import java.util.Arrays;

public class SolverTest {
    private String matrixToString(Matrix m) {
        String res = "";
        for (int i = 0; i < m.getRowDimension(); ++i) {
            for (int j = 0; j < m.getColumnDimension(); ++j) {
                res += Double.toString(m.get(i, j)) + " ";
            }
            res += "\n";
        }
        return res;
    }

    private String failTest(double expected, double taken) {
        String res = "failed - " + Double.toString(expected) + " expected but " + Double.toString(taken) + " taken";
        return res;
    }

    private String failTest(Matrix expected, Matrix taken) {
        String res = "failed - expected\n";
        res += matrixToString(expected) + "but\n" + matrixToString(taken) + "taken";
        return res;
    }

    @Test
    public void testSolverScalar() {
        Matrix left = new Matrix(new double[][] {{1}, {2}, {3}});
        Matrix right = new Matrix(new double[][] {{3}, {2}, {1}});
        double result = Solver.scalar(left, right);
        double expected = 10;
        assertTrue(failTest(expected, result), MyDouble.equals(result, expected));
    }

    @Test
    public void testSolverVectorLength() {
        Matrix vector = new Matrix(new double[][] {{1}, {2}, {3}});
        double result = Solver.vectorLength(vector);
        double expected = 3.741657386773941;
        assertTrue(failTest(expected, result), MyDouble.equals(result, expected));
    }

    @Test
    public void testSolverProjection() {
        Matrix from = new Matrix(new double[][] {{1}, {2}, {3}});
        Matrix to = new Matrix(new double[][] {{3}, {2}, {1}});
        Matrix result = Solver.projection(from, to);
        Matrix expected = new Matrix(new double[][] {{2.142857142857143}, {1.428571428571429}, {0.714285714285714}});
        assertTrue(failTest(expected, result), Solver.equals(result, expected));
    }

    @Test
    public void testSolverGetQ() {
        Solver s = new Solver(new double[][] {
            {3, 2, -5},
            {2, -1, 3},
            {1, 2, -1}
        });

        Matrix result = s.getQ();
        Matrix expected = new Matrix(new double[][] {
            {0.801783725737273,  0.281718084909506, -0.527046276694730},
            {0.534522483824849, -0.732467020764714,  0.421637021355784},
            {0.267261241912424,  0.619779786800912,  0.737864787372622}
        });

        assertTrue(failTest(expected, result), Solver.equals(result, expected));
    }

    @Test
    public void testSolverGetR() {
        Solver s = new Solver(new double[][] {
            {3, 2, -5},
            {2, -1, 3},
            {1, 2, -1}
        });

        Matrix result = s.getR();
        Matrix expected = new Matrix(new double[][] {
            {3.741657386773941,  1.603567451474546, -2.672612419124243},
            {0.000000000000000,  2.535462764185550, -4.225771273642583},
            {0.000000000000000, -0.000000000000000,  3.162277660168380}
        });

        assertTrue(failTest(expected, result), Solver.equals(result, expected));
    }

    @Test
    public void testSolverGetSolver() {
        Solver s = new Solver(new double[][] {
            {3, 2, -5},
            {2, -1, 3},
            {1, 2, -1}
        });

        Matrix result = new Matrix(s.getSolve(new double[] {-1, 13, 9}), 3);
        Matrix expected = new Matrix(new double[] {3, 5, 4}, 3);
 
        assertTrue(failTest(expected, result), Solver.equals(result, expected));
    }
}
