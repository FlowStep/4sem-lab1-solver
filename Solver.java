import Jama.Matrix;

public class Solver {
    private int size;
    private Matrix Q;
    private Matrix R;

    public Solver(double[][] squareMatrix) throws RuntimeException {
        for (double[] line : squareMatrix) {
            if (line.length != squareMatrix.length) {
                throw new RuntimeException("Matrix is not square or lines has different size length");
            }
        }

        size = squareMatrix.length;

        Matrix A = new Matrix(squareMatrix);

        Matrix[] preQ = new Matrix[size];

        // Ортогонализация Грамма-Шмидта
        for (int i = 0; i < size; ++i) {
            Matrix a = A.getMatrix(0, A.getRowDimension() - 1, i, i);
            preQ[i] = a.copy();
            for (int j = i; j > 0; --j) {
                preQ[i].minusEquals(projection(a, preQ[j - 1]));
            }
        }

        // Нормировка
        for (int i = 0; i < size; ++i) {
            preQ[i].arrayRightDivideEquals(new Matrix(preQ[i].getRowDimension(), preQ[i].getColumnDimension(), vectorLength(preQ[i])));
        }

        Q = new Matrix(size, size);

        for (int j = 0; j < size; ++j) {
            for (int i = 0; i < size; ++i) {
                Q.set(i, j, preQ[j].get(i, 0));
            }
        }

        R = Q.transpose().times(A);
    }

    public Matrix getQ() {
        return Q;
    }

    public Matrix getR() {
        return R;
    }

    public static boolean isOneLengthColumns(Matrix left, Matrix right) {
        if (left.getColumnDimension() != 1 || right.getColumnDimension() != 1) {
            return false;
        } else if (left.getRowDimension() != right.getRowDimension()) {
            return false;
        }

        return true;
    }

    public static double scalar(Matrix left, Matrix right) {
        double res = 0;

        for (int i = 0; i < left.getRowDimension(); ++i) {
            res += left.get(i, 0) * right.get(i, 0);
        }

        return res;
    }

    public static double vectorLength(Matrix v) {
        double res = 0;

        for (int i = 0; i < v.getRowDimension(); ++i) {
            res += Math.pow(v.get(i, 0), 2);
        }

        return Math.sqrt(res);
    }

    public static Matrix projection(Matrix from, Matrix to) throws RuntimeException {
        if (!isOneLengthColumns(from, to)) {
            throw new RuntimeException("Vectors are different length");
        }

        Matrix res = to.times((scalar(to, from) / scalar(to, to)));

        return res;
    }

    public static boolean equals(Matrix left, Matrix right) {
        if (left.equals(right))
            return true;

        if (left.getRowDimension() != right.getRowDimension())
            return false;
        
        if (left.getColumnDimension() != right.getColumnDimension())
            return false;

        for (int i = 0; i < left.getRowDimension(); ++i) {
            for (int j = 0; j < left.getColumnDimension(); ++j) {
                if (!MyDouble.equals(left.get(i, j), right.get(i, j)))
                    return false;
            }
        }

        return true;
    }

    public double[] getSolve(double[] f_) {
        Matrix f = new Matrix(f_, size);

        // Q * R * x = f, где
        //   x - столбец неизвестных членов
        //   f - столбец свободных членов
        //   Q - ортогональная матрица (Q * transposed(Q) == I, где I - единичная матрица)
        //   R - верхняя треугольная матрица
        //
        // R * x = transposed(Q) * f

        f = Q.transpose().times(f);

        double[] res = new double[size];

        // Обратный ход Гаусса

        for (int i = R.getRowDimension() - 1; i >= 0; --i) {
            res[i] += f.get(i, 0);

            for (int j = R.getColumnDimension() - 1; j > i; --j) {
                res[i] -= R.get(i, j) * res[j];
            }

            res[i] /= R.get(i, i);
        }

        return res;
    }
}
