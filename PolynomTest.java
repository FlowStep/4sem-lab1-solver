import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class PolynomTest {
    private String failTest(double expected, double taken) {
        String res = "failed - " + Double.toString(expected) + " expected but " + Double.toString(taken) + " taken";
        return res;
    }

    @Test
    public void testPolynomGetValue() {
        Polynom p = new Polynom(new double[] {2, -1, 0, 1});

        double expected = 2405972;
        double result = p.getValue(134);

        assertTrue(failTest(expected, result), MyDouble.equals(result, expected));
    }
}
