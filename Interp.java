public class Interp extends Polynom {
    private double[] arguments;
    private double[] values;

    public Interp(double[] args, double[] vals) {
        double[][] preMembers = new double[args.length][args.length];

        for (int i = 0; i < preMembers.length; ++i) {
            for (int j = 0; j < preMembers[i].length; ++j) {
                preMembers[i][j] = Math.pow(args[i], j);
            }
        }

        Solver members = new Solver(preMembers);

        coefs = members.getSolve(vals);
    }
}
